<?php

$i = 0;

while($i < 10){
    echo ($i);
    echo '<hr>';
    $i++;
}

$y = 10;

do{
    echo($y);
    $y--;
    echo '<hr>';
} while($y > 0);

$z = 10;

for($z = 0; $z < 10; $z++){
    echo ($z);
    echo '<hr>';
}

$a = [0,1,2,3,4,5,6,7,8,9,0];

foreach($a as $n){
    echo ($n);
    echo '<hr>';
}